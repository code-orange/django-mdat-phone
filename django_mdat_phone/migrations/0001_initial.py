# Generated by Django 3.1.7 on 2021-04-06 12:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("django_mdat_location", "0005_change_city_name_to_250_chars"),
    ]

    operations = [
        migrations.CreateModel(
            name="MdatPhoneNationalCode",
            fields=[
                ("id", models.BigAutoField(primary_key=True, serialize=False)),
                ("national_code", models.IntegerField()),
                (
                    "city",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="django_mdat_location.mdatcities",
                    ),
                ),
            ],
            options={
                "db_table": "mdat_phone_national_code",
                "unique_together": {("city", "national_code")},
            },
        ),
    ]
