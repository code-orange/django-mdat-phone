from django_mdat_location.django_mdat_location.models import *


class MdatPhoneNationalCode(models.Model):
    id = models.BigAutoField(primary_key=True)
    city = models.ForeignKey(MdatCities, models.DO_NOTHING)
    national_code = models.IntegerField()

    class Meta:
        db_table = "mdat_phone_national_code"
        unique_together = (("city", "national_code"),)


class MdatPhoneCountryCode(models.Model):
    id = models.BigAutoField(primary_key=True)
    country = models.ForeignKey(MdatCountries, models.DO_NOTHING)
    country_code = models.IntegerField()

    class Meta:
        db_table = "mdat_phone_country_code"
        unique_together = (("country", "country_code"),)
